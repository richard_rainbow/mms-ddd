package com.ccb.mms.modelcontext.application;

import com.ccb.mms.modelcontext.application.assembler.ModelAssembler;
import com.ccb.mms.modelcontext.application.pl.ModelDto;
import com.ccb.mms.modelcontext.domain.model.entity.Model;
import com.ccb.mms.modelcontext.domain.model.service.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author hongyang
 * @version v1.0.0
 * @date 2021/3/8 6:21 下午
 */
@Service
public class ModelAppServiceImpl implements ModelAppService {

    @Autowired
    private ModelService modelService;

    @Override
    public void registerModel(ModelDto modelDto) {
        Model model = ModelAssembler.assembleModel(modelDto);
        modelService.registerModelInfo(model);
    }
}
