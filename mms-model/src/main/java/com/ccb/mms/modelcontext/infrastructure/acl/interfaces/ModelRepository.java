package com.ccb.mms.modelcontext.infrastructure.acl.interfaces;

import com.ccb.mms.modelcontext.domain.model.entity.Model;
import com.ccb.mms.core.stereotype.Port;
import com.ccb.mms.core.stereotype.PortType;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author hongyang
 * @version v1.0.0
 * @date 2021/3/8 2:17 下午
 */
@Mapper
@Repository
@Port(PortType.Repository)
public interface ModelRepository {

    /**
     * 新增模型信息
     * @param model 模型信息
     * @return 结果
     */
     int addModel(Model model);

    /**
     * 通过模型ID删除模型
     *
     * @param modelId 模型ID
     * @return 结果
     */
    int deleteModelById(Long modelId);
}
