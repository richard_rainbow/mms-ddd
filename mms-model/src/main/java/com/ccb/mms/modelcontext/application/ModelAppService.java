package com.ccb.mms.modelcontext.application;

import com.ccb.mms.modelcontext.application.pl.ModelDto;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author hongyang
 * @version v1.0.0
 * @date 2021/3/8 12:21 下午
 */
public interface ModelAppService {

    void registerModel(ModelDto modelDto);
}
