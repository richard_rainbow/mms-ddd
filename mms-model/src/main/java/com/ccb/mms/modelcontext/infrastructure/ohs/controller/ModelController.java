package com.ccb.mms.modelcontext.infrastructure.ohs.controller;

import com.ccb.mms.core.stereotype.Remote;
import com.ccb.mms.core.stereotype.RemoteType;
import com.ccb.mms.modelcontext.application.ModelAppService;
import com.ccb.mms.modelcontext.application.pl.ModelDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author hongyang
 * @version v1.0.0
 * @date 2021/3/8 5:28 下午
 */
@RestController
@RequestMapping("/model")
@Remote(RemoteType.Controller)
public class ModelController {

    @Autowired
    private ModelAppService modelAppService;

    void register(ModelDto modelDto) {
        modelAppService.registerModel(modelDto);
    }

}
