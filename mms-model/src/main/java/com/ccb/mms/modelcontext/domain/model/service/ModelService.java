package com.ccb.mms.modelcontext.domain.model.service;

import com.ccb.mms.modelcontext.domain.model.entity.Model;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author hongyang
 * @version v1.0.0
 * @date 2021/3/8 3:46 下午
 */
public interface ModelService {

    void registerModelInfo(Model model);
}
