package com.ccb.mms.modelcontext.domain.model.service;

import com.ccb.mms.core.stereotype.DomainService;
import com.ccb.mms.modelcontext.domain.model.entity.Model;
import com.ccb.mms.modelcontext.infrastructure.acl.interfaces.ModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author hongyang
 * @version v1.0.0
 * @date 2021/3/8 3:41 下午
 */
@DomainService
@Service
public class ModelServiceImpl implements ModelService {

    @Autowired
    private ModelRepository modelRepository;


    @Override
    public void registerModelInfo(Model model) {
        modelRepository.addModel(model);
    }
}
