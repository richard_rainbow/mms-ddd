package com.ccb.mms.core.stereotype;

/**
 * @author hongyang
 */
public enum PortType {
    Repository,
    Client,
    Publisher
}
