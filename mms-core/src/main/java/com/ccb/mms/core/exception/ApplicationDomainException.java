package com.ccb.mms.core.exception;

/**
 * @author hongyang
 */
public class ApplicationDomainException extends ApplicationException {
    public ApplicationDomainException(String message, Exception ex) {
        super(message, ex);
    }
}