package com.ccb.mms.core.exception;

/**
 * @author hongyang
 */
public class ApplicationValidationException extends ApplicationException {
    public ApplicationValidationException(String message) {
        super(message);
    }
}