package com.ccb.mms.core.exception;

/**
 * @author hongyang
 */
public class ApplicationInfrastructureException extends ApplicationException {
    public ApplicationInfrastructureException(String message, Exception ex) {
        super(message, ex);
    }
}