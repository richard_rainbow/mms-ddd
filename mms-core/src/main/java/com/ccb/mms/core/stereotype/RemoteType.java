package com.ccb.mms.core.stereotype;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author hongyang
 * @version v1.0.0
 * @date 2021/3/8 5:30 下午
 */
public enum RemoteType {
    Resource,
    Controller,
    Provider,
    Subscriber
}
